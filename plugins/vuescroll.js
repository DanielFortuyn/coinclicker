import Vue from 'vue'
import vuescroll from 'vuescroll'
import 'vuescroll/dist/vuescroll.css'



Vue.use(vuescroll, {
    ops: {
        vuescroll: {},
        scrollPanel: {
            scrollingX: false,
        },
        rails: {
            background: '#ff007f'
        },
        bar: {}
    }
})