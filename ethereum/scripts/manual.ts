import { ContractFactory } from "ethers";
import { run, ethers } from "hardhat";
import {libs} from "./definitions";
const upgrades = require("../../helpers/upgrades.js");

async function main() {
    await run("compile");

    const accounts = await ethers.getSigners();

    let linkedLibs: { [name: string]: string } = {};
    let lib;
    let factory: ContractFactory;

    // for (let name in libs) {
    //     lib = libs[name];
    //     if(lib.address == '') {
    //         if (!lib.libraries) {
    //             factory = await ethers.getContractFactory(name);
    //         } else {
    //             console.log('Linking libs', lib.libraries);
    //             linkedLibs = {};
    //             for (let link of lib.libraries) {
    //                 linkedLibs[link] = libs[link].address
    //             }
    //             factory = await ethers.getContractFactory(name, {
    //                 libraries: linkedLibs
    //             });
    //         }

    //         libs[name].instance = await factory.deploy();
    //         var receipt = await libs[name].instance.deployed();
    //         if (receipt.status == 0) { throw new Error("failed to deploy"); }
            
    //         libs[name].address = libs[name].instance.address;
    //         console.log(name + " deployed to:", libs[name].address);
    //     }

    // }

    console.log('All deployed, setting proper contract relations');

    if(libs.Main.instance) {
        // const initTx = await libs.Main.instance.initialize();
        // await initTx.wait();
        console.log('Init main');
        
        const setGreetingTx = await libs.Main.instance.setContracts("0x40d8E3dE5a5a2CC8c5f5971dFF315D2c04Be9819", "0x9ba3b23b32ABD1E30C6fC9c80F4A131624D071Da");
        // wait until the transaction is mined
        await setGreetingTx.wait();

        // const initTx2 = await libs.Vars.instance.initialize();
        // const initTx3 = await libs.Tokens.instance.initialize(libs['Main'].address);
        // wait until the transaction is mined
        // await initTx2.wait();
        // await initTx3.wait();

    }

    //     const setGreetingTx = await libs.Main.instance.setContracts(libs.Tokens.address, libs.Vars.address);
    //     // wait until the transaction is mined
    //     await setGreetingTx.wait();

    //     console.log('Inserting upgrades..');
    //     let upgradeData = [];
    //     let keys = [];
    
    //     for (let upgrade of upgrades) {
    //         let c = upgrade.contract;
    //         let upgDep = c.dependencies.upgrade || 0;
    //         let achDep = c.dependencies.achievement || 0;
    //         let building = c.building || 0;
    //         let level = c.level || 0;
    
    //         upgradeData.push([c.typeId, building, upgDep, achDep, level, c.price, c.value]);
    //         keys.push(upgrade.id);
    //     }
    //     const tx = await libs.Vars.instance.addUpgrades(keys, upgradeData); 
    //     let res = await tx.wait();

    //     console.log("Insert achievement values"); 
    // }

    console.log('---------------------COPYTODOTENV-----------------');
    console.log("CONTRACT_ADDRESS_MAIN:\"" + libs['Main'].address + "\",");
    console.log("CONTRACT_ADDRESS_VARS:\"" + libs['Vars'].address + "\",");
    console.log("CONTRACT_ADDRESS_TOKENS:\"" + libs['Tokens'].address + "\"")

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
