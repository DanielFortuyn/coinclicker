import { ContractFactory } from "ethers";
import { run, ethers } from "hardhat";
import {libs} from "./definitions";
import mainart from "../artifacts/contracts/Main.sol/Main.json";
import varsart from "../artifacts/contracts/Vars.sol/Vars.json";
import tokensabi from "../artifacts/contracts/Tokens.sol/Tokens.json";
const upgrades = require("../../helpers/upgrades.js");
const contracts = require("../../helpers/contracts.js");

async function main() {


    const accounts = await ethers.getSigners();
    const Main = new ethers.Contract(contracts.CONTRACT_ADDRESS_MAIN, mainart.abi, accounts[0]);
    const Vars = new ethers.Contract(contracts.CONTRACT_ADDRESS_VARS, varsart.abi,  accounts[0]);
    const Tokens = new ethers.Contract(contracts.CONTRACT_ADDRESS_TOKENS, tokensabi.abi,  accounts[0]);

    // const tx = await Main.transferBalance(160); 
    // await tx.wait();

    // const setAchievements = await vars.setAchievements([
    //     100000,10000000,50000000,10000000000,500000000000,10000000000,100000000000,100000000000,50000000000,9000000000,
    //     1000,10000,50000,100000,500000,1000000,10000000,100000000,1000000000,10000000000,
    //     1,5,10,20,30,40,50,60,80,100,
    //     10,20,30,40,50,60,80,100,111,256,
    //     5,10,20,50,5,10,20,50,5,10, 
    //     20,50,5,10,20,50,5,10,20,50, //52 WP
    //     5,10,20,50,5,10,20,50,5,10, 
    //     20,50,5,10,20,50,5,10,20,50,
    //     5,10,20,50,5,10,20,50,5,10, 
    //     20,50,5,10,20,50,5,10,20,50,
    //     5,10,20,50,5,10,20,50,5,10, 
    //     20,50,5,10,20,50,5,10,20,50,
    //     5,10,20,50,5,10,20,50,0,10, // 129
    //     0,1,2,3,4,5,6,7,8,9, // 130 - 139 level
    //     1,10,100,1000,5000,10000,50000,100000,250000,1000000, // CGT 140 - 149
    //     1,100,10000,1000000,100000000,100000000,10000000000,100000000000,1000000000000,1000000000000, // 150 - 159 AGE
    //     1,2,10,100,1000,10000,0,0,0,0, // 160 - 169 Negative production
    //     10000,20000,500000,1000000,50000000,0,0,0,0,0, // 170 - 179 Negative bank
    //     1,5,10,20,50,0,0,0,0,0, // 189
    //     0,0,0,0,0,0,0,0,0,0, // 199
    //     0,0,0,0,0,0,0,0,0,0, // 209
    //     0,0,0,0,0,0,0,0,0,0, // 219
    //     0,0,0,0,0,0,0,0,0,0, // 229
    //     0,0,0,0,0,0,0,0,0,0, // 239
    //     0,0,0,0,0,0,0,0,0,0, // 249
    //     0,0,0,0,0,0 // 256
    // ]);

    // setAchievements.wait();

    // const tx3 = await tokens.addNft(contracts.CONTRACT_ADDRESS_MAIN, 1, 999999999999999, 9);
    // tx3.wait();

    // const tx4 = await main.setCoins();
    // tx4.wait();

    // console.log('---------------------COPYTODOTENV-----------------');

    if(Main) {
        // const initTx = await Main.initialize();
        // console.log('Main init');
        // const initTx2 = await Vars.initialize();
        // console.log('Var init');
        // const initTx3 = await Tokens.initialize(contracts.CONTRACT_ADDRESS_MAIN);
        // console.log('Token init');
        // // wait until the transaction is mined
        // await initTx.wait();
        // await initTx2.wait();
        // await initTx3.wait();

        // console.log('Set Contracts')
        // const setGreetingTx = await Main.setContracts(contracts.CONTRACT_ADDRESS_TOKENS, contracts.CONTRACT_ADDRESS_VARS);
        // wait until the transaction is mined
        // await setGreetingTx.wait();

        // console.log('Inserting upgrades..');
        // let upgradeData = [];
        // let keys = [];
    
        // for (let upgrade of upgrades) {
        //     let c = upgrade.contract;
        //     let upgDep = c.dependencies.upgrade || 0;
        //     let achDep = c.dependencies.achievement || 0;
        //     let building = c.building || 0;
        //     let level = c.level || 0;
    
        //     upgradeData.push([c.typeId, building, upgDep, achDep, level, c.price, c.value]);
        //     keys.push(upgrade.id);
        // }
        // const tx = await Vars.addUpgrades(keys, upgradeData); 
        // let res = await tx.wait();

        // console.log("Insert achievement values"); 

        await Main.setLevel("0x4b8E01489EA94ac9dc660848A484C7Edb9cE5ec8");
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });


        // "100000","10000000","50000000","10000000000","500000000000","100000000000000","10000000000000000","1000000000000000000","5000000000000000000","9000000000000000000",
    // "1000","10000","50000","100000","500000","1000000","10000000","100000000","1000000000","100000000000",
    // "1","5","10","20","30","40","50","60","80","100",
    // "10","20","30","40","50","60","80","100","111","256",
    // "5","10","20","50","5","10","20","50","5","10", 
    // "20","50","5","10","20","50","5","10","20","50", //52 WP
    // "5","10","20","50","5","10","20","50","5","10", 
    // "20","50","5","10","20","50","5","10","20","50",
    // "5","10","20","50","5","10","20","50","5","10", 
    // "20","50","5","10","20","50","5","10","20","50",
    // "5","10","20","50","5","10","20","50","5","10", 
    // "20","50","5","10","20","50","5","10","20","50",
    // "5","10","20","50","5","10","20","50","0","10", // 129
    // "0","1","2","3","4","5","6","7","8","9", // 130 - 139 level
    // "1","10","100","1000","5000","10000","50000","100000","250000","1000000", // CGT 140 - 149
    // "1","100","10000","1000000","100000000","100000000","10000000000","100000000000","1000000000000","1000000000000", // 150 - 159 AGE
    // "1","2","10","100","1000","10000","0","0","0","0", // 160 - 169 Negative production
    // "10000","20000","500000","1000000","50000000","0","0","0","0","0", // 170 - 179 Negative bank
    // "1","5","10","20","50","0","0","0","0","0", // 189
    // "0","0","0","0","0","0","0","0","0","0", // 199
    // "0","0","0","0","0","0","0","0","0","0", // 209
    // "0","0","0","0","0","0","0","0","0","0", // 219
    // "0","0","0","0","0","0","0","0","0","0", // 229
    // "0","0","0","0","0","0","0","0","0","0", // 239
    // "0","0","0","0","0","0","0","0","0","0", // 249
    // "0","0","0","0","0","0" // 256