import { ContractFactory } from "ethers";
import { run, ethers } from "hardhat";
import {libs} from "./definitions";


async function main() {

    let libs = {
        Tokens: {
            address: "0xe46f65257893A96971785b5Ce20c35a81741C0d9"
        },
        Vars: {
            address: "0xE3bEa38d87D24F507182d864F3DB7f9A7a0e51D1"
        },
        Main: {
            address: "0x6494880AB283172b7D621AEFBd52dB33F207F84C"
        }
    }

    // if(libs.Main.instance) {
        libs.Tokens.instance =  new ethers.Contract("0x9E545E3C0baAB3E08CdfD552C960A1050f373042", libs.Tokens); 
        const initTx3 = await libs.Tokens.instance.initialize("0x9E545E3C0baAB3E08CdfD552C960A1050f373042");
        // wait until the transaction is mined
        await initTx3.wait();
        
        const setGreetingTx = await libs.Main.instance.setContracts(libs.Tokens.address, libs.Vars.address);
        // wait until the transaction is mined
        await setGreetingTx.wait();
    }

    console.log('---------------------COPYTODOTENV-----------------');
    console.log("CONTRACT_ADDRESS_MAIN:\"" + libs['Main'].address + "\",");
    console.log("CONTRACT_ADDRESS_VARS:\"" + libs['Vars'].address + "\",");
    console.log("CONTRACT_ADDRESS_TOKENS:\"" + libs['Tokens'].address + "\"")

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
