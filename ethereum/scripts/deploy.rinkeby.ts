import { ContractFactory } from "ethers";
import { run, ethers } from "hardhat";
import {libs} from "./definitions";

async function main() {
    await run("compile");

    const accounts = await ethers.getSigners();

    console.log(
        "Accounts:",
        accounts.map((a) => a.address)
    );
    let linkedLibs: { [name: string]: string } = {};
    let lib;
    let factory: ContractFactory;

    for (let name in libs) {
        lib = libs[name];
        if (!lib.libraries) {
            factory = await ethers.getContractFactory(name);
        } else {
            // console.log('Linking libs', lib.libraries);
            linkedLibs = {};
            for (let link of lib.libraries) {
                linkedLibs[link] = libs[link].address
            }
            factory = await ethers.getContractFactory(name, {
                libraries: linkedLibs
            });
        }

        libs[name].instance = await factory.deploy();
        libs[name].address = libs[name].instance.address;
        // console.log(name + " deployed to:", libs[name].address);

    }

    console.log('All deployed, setting proper contract relations');

    if(libs.Main.instance) {
        const initTx = await libs.Main.instance.initialize();
        const initTx2 = await libs.Vars.instance.initialize();

        // wait until the transaction is mined
        await initTx.wait();
        await initTx2.wait();


        const setGreetingTx = await libs.Main.instance.setContracts("0x8514e33bCEC6aD21c6aB1cE38548F9E0478751Fe", libs.Vars.address);
        // wait until the transaction is mined
        await setGreetingTx.wait();
    }

    console.log('---------------------COPYTODOTENV-----------------');
    console.log("CONTRACT_ADDRESS_MAIN:\"" + libs['Main'].address + "\",");
    console.log("CONTRACT_ADDRESS_VARS:\"" + libs['Vars'].address + "\",");
    console.log("CONTRACT_ADDRESS_TOKENS:\"" + libs['Tokens'].address + "\"")

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
