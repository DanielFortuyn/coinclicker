import { ContractFactory } from "ethers";

type DeployContract = {
    name: string,
    address: string,
    libraries?: Array<string>,
    factory?: ContractFactory,
    instance?:  any
};

export let libs: { [name: string]: DeployContract } = {
    NumHouses: {
        name: 'NumHouses',
        address: ''
    },
    GetCoins: {
        name: 'GetCoins',
        address: ''
    },
    CoinMath: {
        name: 'CoinMath',
        address: ''
    },
    GetAchievement: {
        name: 'GetAchievement',
        address: ''
    },
    ClaimAchievement: {
        name: 'ClaimAchievement',
        address: '',
        libraries: ['GetCoins', 'GetAchievement']
    },
    ClaimRandomEvent: {
        name: 'ClaimRandomEvent',
        address: ''
    },
    ClaimGovernanceToken: {
        name: 'ClaimGovernanceToken',
        address: '',
        libraries: ['GetAchievement']
    },
    LevelUp: {
        name: 'LevelUp',
        address: '',
        libraries: ['GetCoins']
    },
    Buy: {
        name: 'Buy',
        address: '',
        libraries: ['GetCoins', 'CoinMath']
    },
    BuyUpgrade: {
        name: 'BuyUpgrade',
        address: '',
        libraries: ['GetCoins', 'GetAchievement']
    },
    Sell: {
        name: 'Sell',
        address: '',
        libraries: []
    },
    PullRug: {
        name: 'PullRug',
        address: '',
        libraries: ['GetCoins','NumHouses']
    },
    CreateGame: {
        name: 'CreateGame',
        address: '',
        libraries: ['NumHouses']
    },
    Vars: {
        name: 'Vars',
        address: '',
    },
    
    Tokens: {
        name: 'Tokens',
        address: '0x4fd4d1a77743047179FB8c7e5Ad5eDA104A796FE',
    },
    Main: {
        name: 'Main',
        address: '',
        libraries: ['Buy','Sell','BuyUpgrade','GetCoins','CreateGame','PullRug','LevelUp', 'ClaimAchievement', 'ClaimGovernanceToken', 'ClaimRandomEvent']
    }
};