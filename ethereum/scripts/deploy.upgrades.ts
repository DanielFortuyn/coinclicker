import { ContractFactory } from "ethers";
import { run, ethers } from "hardhat";
import {libs} from "./definitions";
const upgrades = require("../../helpers/upgrades.js");


async function main() {
    let addresses = {
        Tokens: {
            address: "0xe46f65257893A96971785b5Ce20c35a81741C0d9"
        },
        Vars: {
            address: "0xE3bEa38d87D24F507182d864F3DB7f9A7a0e51D1"
        },
        Main: {
            address: "0xabebE9a2D62Af9a89E86EB208b51321e748640C3"
        }
    }

    console.log('Inserting upgrades..');

    let upgradeData = [];
    let keys = [];

    for (let upgrade of upgrades) {
        let c = upgrade.contract;
        let upgDep = c.dependencies.upgrade || 0;
        let achDep = c.dependencies.achievement || 0;
        let building = c.building || 0;
        let level = c.level || 0;

        upgradeData.push([c.typeId, building, upgDep, achDep, level, c.price, c.value]);
        keys.push(upgrade.id);
    }

    const MyContract = await ethers.getContractFactory("Vars");
    const contract = await MyContract.attach(
        addresses.Vars.address
    );


    const tx = await contract.addUpgrades(keys, upgradeData); 
    let res = await tx.wait();


    console.log('---------------------COPYTODOTENV-----------------');
    console.log("CONTRACT_ADDRESS_MAIN:\"" + libs['Main'].address + "\",");
    console.log("CONTRACT_ADDRESS_VARS:\"" + libs['Vars'].address + "\",");
    console.log("CONTRACT_ADDRESS_TOKENS:\"" + libs['Tokens'].address + "\"")

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
