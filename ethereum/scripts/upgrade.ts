import { ContractFactory } from "ethers";
import { run, ethers } from "hardhat";
import {libs} from "./definitions";
import mainart from "../artifacts/contracts/Main.sol/Main.json";
import varsart from "../artifacts/contracts/Vars.sol/Vars.json";
import tokensabi from "../artifacts/contracts/Tokens.sol/Tokens.json";
const contracts = require("../../helpers/contracts.js");
const upgradeSourceData = require("../../helpers/upgrades.js");

// const newcontracts = require("../../helpers/contracts.new.js");


async function main() {
    const accounts = await ethers.getSigners();

    const Main = new ethers.Contract(contracts.CONTRACT_ADDRESS_MAIN, mainart.abi, accounts[0]);
    const Vars = new ethers.Contract(contracts.CONTRACT_ADDRESS_VARS, varsart.abi,  accounts[0]);
    const Tokens = new ethers.Contract(contracts.CONTRACT_ADDRESS_TOKENS, tokensabi.abi,  accounts[0]);

    // const NewMain = new ethers.Contract(newcontracts.CONTRACT_ADDRESS_MAIN, mainart.abi, accounts[0]);

    // const setGreetingTx = await Main.setContracts("0x40d8E3dE5a5a2CC8c5f5971dFF315D2c04Be9819", "0x9ba3b23b32ABD1E30C6fC9c80F4A131624D071Da");
    // await setGreetingTx.wait();

    // const vx = await Vars.initialize();
    // const r = await vx.wait();

    //INSERT UPGRADES
    // console.log('Inserting upgrades..');
    // let upgradeData = [];
    // let keys = [];

    // for (let upgrade of upgradeSourceData) {
    //     let c = upgrade.contract;
    //     let upgDep = c.dependencies.upgrade || 0;
    //     let achDep = c.dependencies.achievement || 0;
    //     let building = c.building || 0;
    //     let level = c.level || 0;

    //     upgradeData.push([c.typeId, building, upgDep, achDep, level, c.price, c.value]);
    //     keys.push(upgrade.id);
    // }
    // const tx = await Vars.addUpgrades(keys, upgradeData); 
    // let res = await tx.wait();


    // console.log('Transfer balance back');
    // const tx1 = await Main.transferBalance(160);
    // tx1.wait();

    // let addresses = ["0xaa33d2b155a35484830d449b0ee092739d27b298","0x43defa20dedd151e9e9f9a765d70da5089299fb0","0x9a9885ff1f94ac6958488d2f1473f6e702fc056d","0x96f005ab51df78207dd6ae146143ed3cc3efcc42","0xc5e7bd53c4b7c559cf2628db1aeca0fdf14fb369","0x49db82ff7b06976f2ab6b69c5d23a6c7ee0bcea3","0x99b0a6e6bddb11992ee39152df093c7753816fd5",
    // "0xb16719bc309cbeddd638dc01bbf22dae9be38d2d"];

    // let element = "0xb16719bc309cbeddd638dc01bbf22dae9be38d2d";
    // console.log("Migrate " + element);
    // const result = await Main.getGame(element);
    // const tx2 = await NewMain.setGame(element, result);
    // tx2.wait();

    console.log('Transferring nfts into contract..')
    let ids = [];
    let ownaddress = [];
    let address = await accounts[0].getAddress();
    for(let i = 0; i<=160; i++) {
        ownaddress.push(address);
        ids.push(i);
    }
    console.log("Transferrint balances  to new contract");
    let tokenBalances = await Tokens.balanceOfBatch(ownaddress,ids);
    let result = await Tokens.safeBatchTransferFrom(address, contracts.CONTRACT_ADDRESS_MAIN, ids, tokenBalances, "0x00");
    console.log(result);
    console.log('finished');
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
