import { expect } from 'chai'
import { run, ethers } from "hardhat";
import { Signer,ContractFactory } from "ethers";
import {libs} from "../scripts/definitions";

let linkedLibs: { [name: string]: string } = {};

describe("Main", function () {
    let accounts: Signer[];

    beforeEach(async function () {
        accounts = await ethers.getSigners();
        let lib;
        let factory: ContractFactory;

        for (let name in libs) {
            lib = libs[name];
            if (!lib.libraries) {
                factory = await ethers.getContractFactory(name);
            } else {
                linkedLibs = {};
                for (let link of lib.libraries) {
                    linkedLibs[link] = libs[link].address
                }
                factory = await ethers.getContractFactory(name, {
                    libraries: linkedLibs
                });
            }
            libs[name].instance = await factory.deploy();
            libs[name].address = libs[name].instance.address;
        }
        if (libs.Main.instance) {
            const initTx = await libs.Main.instance.initialize();
            const initTx2 = await libs.Vars.instance.initialize();

            // wait until the transaction is mined
            await initTx.wait();
            await initTx2.wait();

            const setGreetingTx = await libs.Main.instance.setContracts(libs.Tokens.address, libs.Vars.address);
            // wait until the transaction is mined
            await setGreetingTx.wait();
        }
    });

    it("Should be able ot create a game and return one token", async function () {
        const createGameTx = await libs.Main.instance.createGame();
        // wait until the transaction is mined
        let coins = 0;
        await createGameTx.wait();
        expect(await libs.Main.instance.getCoins()).to.equal(5);
        await ethers.provider.send("evm_mine", []); 
        await ethers.provider.send("evm_mine", []); 
        
        coins = await libs.Main.instance.getCoins();
        expect(await libs.Main.instance.getCoins()).to.equal(15);



    });

    it("Should have more tokens after 50 ticks", async function () {
        const createGameTx = await libs.Main.instance.createGame();
        // wait until the transaction is mined
        await createGameTx.wait();
        for(let i=0; i<=50; i++) {
            await ethers.provider.send("evm_mine", []); 
        }
        expect(await libs.Main.instance.getCoins()).to.above(0);
    });

    it("Should be able to buy an upgrade after some time", async function () {
        const createGameTx = await libs.Main.instance.createGame();
        // wait until the transaction is mined
        await createGameTx.wait();
        for(let i=0; i<=50; i++) {
            await ethers.provider.send("evm_mine", []); 
        }
        await libs.Main.instance.buyUpgrade(199);

        let game = await libs.Main.instance.getGame(accounts[0].getAddress());
        expect(game.upgrades[199]).to.above(50);
    })
});
