import '@typechain/hardhat'
import '@nomiclabs/hardhat-ethers'
import "@nomiclabs/hardhat-etherscan";
import '@nomiclabs/hardhat-waffle'
import * as dotenv from "dotenv";
dotenv.config({ path: __dirname+'/.env' });
import { HardhatUserConfig, HardhatConfig } from "hardhat/types";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import 'hardhat-contract-sizer';

const key = process.env.KEY as string;
const ankr = process.env.ANKR as string;
const mnemonic = process.env.MNEMONIC as string;

const config: HardhatUserConfig = {
  solidity: {
    compilers: [{ version: "0.8.0", settings: {
      optimizer: {
      enabled: true,
      runs: 1000
    }}}]
  },
  contractSizer: {
    alphaSort: true,
    // runOnCompile: true,
    disambiguatePaths: false,
  },
  networks: {
    hardhat: {
      chainId: 91,
      mining: {
        auto: true,
        interval: [1000,1001]
      }
    },
    // FANTOM_TESTNET: {
    //   url: "https://rpc.testnet.fantom.network",
    //   accounts: {mnemonic: mnemonic}
    // },
    // FANTOM_OPERA: {
    //   chainId: 250,
    //   url: "https://rpc.fantom.network",
    //   accounts: {mnemonic: mnemonic}
    // }
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: process.env.FTMSCAN_KEY
  }
};
export default config;