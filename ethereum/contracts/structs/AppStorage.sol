// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./Game.sol";
import "./Upgrade.sol";
import "../Vars.sol";
import "../Tokens.sol";

struct AppStorage {
    Vars vars;
    Tokens tokens;
    uint sunrise;
    uint bananas;

    address[] gameIndex; 

    int16[22] profit;
    uint64[22] price;
    uint64[10] levelPrice;
    uint16 bavailibility;
    uint16 governanceAchievementReward;
    uint16 governanceBlockHeight;

    mapping(address => Game) games;
    uint8[256] upgrades;

    uint random;
    uint gameCount;
}