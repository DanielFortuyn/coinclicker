// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

struct Game {
    address owner;
    uint8 level;
    uint claimed;
    uint256 start;
    uint256 achievements;
    int256 tokens;
    int32[22] profit;
    uint32[22] buildingAmount;
    uint256[22] buildingSum;
    uint32[256] upgrades;
    uint256 random;
    uint256[22] buildingCost;
}