// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

struct Upgrade {
    uint8 typeId;
    uint8 building;
    int16 upgDep;
    int16 achDep;
    uint8 level;
    uint32 price;
    int64 value;
}
