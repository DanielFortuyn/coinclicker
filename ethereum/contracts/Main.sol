// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "./structs/AppStorage.sol";
import "./structs/Game.sol";

import "./libraries/Buy.sol";
import "./libraries/Sell.sol";
import "./libraries/CreateGame.sol";
import "./libraries/GetCoins.sol";
import "./libraries/BuyUpgrade.sol";
import "./libraries/LevelUp.sol";
import "./libraries/PullRug.sol";
import "./libraries/ClaimAchievement.sol";
import "./libraries/ClaimGovernanceToken.sol";
import "./libraries/ClaimRandomEvent.sol";

contract Main is Initializable, ERC1155HolderUpgradeable, OwnableUpgradeable {
    AppStorage internal s;

    using Buy for Game;
    using Sell for Game;
    using BuyUpgrade for Game;
    using GetCoins for Game;
    using LevelUp for Game;
    using PullRug for Game;
    using ClaimAchievement for Game;
    using ClaimGovernanceToken for Game;
    using ClaimRandomEvent for Game;

    function initialize() public initializer {
        __Ownable_init_unchained();
        s.sunrise = block.number;
        s.random = uint(blockhash(block.number-1));
    }
    
    function createGame() public {
        CreateGame.createGame(s, s.random);
        s.random = uint(blockhash(block.number-1));
    }

    function buy(uint8 _buildingId) public {
        Game storage g = s.games[msg.sender];
        g.buy(s, _buildingId);
    }

    function sell(uint8 _buildingId) public {
        Game storage g = s.games[msg.sender];
        g.sell(s, _buildingId);
    }

    function getCoins() public view returns(int256) {
        Game storage g = s.games[msg.sender];
        return g.getCoins();
    }

    function buyUpgrade(uint8 _upgradeId) public {
        Game storage g = s.games[msg.sender];
        g.buyUpgrade(s, _upgradeId);
    }

    function pullRug(uint8 _rugId) public {
        Game storage g = s.games[msg.sender];
        g.pullRug(s, _rugId);
    }

    function levelUp() public {
        Game storage g = s.games[msg.sender];
        g.levelUp(s);
    }

    function getGame(address _a) public view returns(Game memory) {
        return s.games[_a];
    }

    function claimAchievement(uint8 _achievementId) public {
        Game storage g = s.games[msg.sender];
        g.claimAchievement(s, _achievementId);
    }

    function claimGovernanceToken() public {
        Game storage g = s.games[msg.sender];
        g.claimGovernanceToken(s);
    }

    function banance() public view returns(uint) {
        return s.bananas;
    }

    function sunrise() public view returns(uint) {
        return s.sunrise;
    }

    function claimRandomEvent() public {
        Game storage g = s.games[msg.sender];
        g.claimRandomEvent(s);
    }

    //Reclaim unclaimed NFTS and CCGT in case of upgrade;
    function transferBalance(uint _length) public onlyOwner {
        uint[] memory tokenIds = new uint[](_length);
        address[] memory addresses = new address[](_length);
        for (uint32 i = 0; i < _length; i++) {
            tokenIds[i] = i;
            addresses[i] = address(this);
        }
        s.tokens.safeBatchTransferFrom(address(this), owner(), tokenIds, s.tokens.balanceOfBatch(addresses, tokenIds), "0x0");
    }

    function setContracts(Tokens _tokens, Vars _vars) public onlyOwner {
        s.tokens = _tokens;
        s.vars = _vars;
    }

    function getPlayers() public view returns (address[] memory players) {
        players = s.gameIndex;
    }
    function getNumPlayers() public view returns (uint players) {
        players = s.gameCount;
    }

    //Updateability
    function setLevel(address _a) public onlyOwner {
        Game storage g = s.games[_a];
        g.achievements = g.achievements | (uint256(1) << 250);
        g.buildingAmount[21] = 1;
        g.level = 3;
        g.tokens = 10000000000;
    }
}
