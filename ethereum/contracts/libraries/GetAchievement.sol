// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";

library GetAchievement {
    function getAchievement(Game storage g, uint _boolNumber) public view returns (bool) { 
        uint256 flag = (g.achievements >> _boolNumber) & uint256(1);
        return (flag == 1 ? true : false);
    } 
}