// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "./GetCoins.sol";
import "./CoinMath.sol";

library LevelUp {
    using GetCoins for Game;

    function levelUp(Game storage _g, AppStorage storage _s) external {
        require(
            int256(_g.getCoins()) >= int(_s.vars.getLevelPrice()[uint256(_g.level + 1)])
        );
        uint32[22] memory buildingAmount;
        uint256[22] memory buildingSum;
        uint256[22] memory buildingCost;
        uint32[128] memory upgrades;
        uint32 numHouses = _g.buildingAmount[0];

        _g.upgrades = upgrades;
        _g.level++;
        _g.tokens = 0;
        _g.buildingAmount = buildingAmount;
        _g.buildingSum = buildingSum;
        _g.buildingCost = buildingCost;
        _g.profit = _s.vars.getProfit();
        _g.start = block.number;
        _g.buildingAmount[0] = numHouses;
    }
}