// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
library GetCoins {
    function getCoins(Game storage _g) public view returns (int256 total) {
        int256 diff = int(block.number) - int(_g.start);
        total = _g.tokens;
        for (uint256 i = 0; i < 22; i++) {
            if(_g.buildingAmount[i] > 0) {
                int256 diffA = (diff * _g.buildingAmount[i]) - int(_g.buildingSum[i]);
                int256 cLevel = uint16(_g.level + 1);
                total = (total - int(_g.buildingCost[i])) + (int256(diffA) * _g.profit[i] * int256(cLevel));
            }
        }
    } 
}