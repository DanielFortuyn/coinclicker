// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "../structs/Upgrade.sol";
import "./GetCoins.sol";
import "./GetAchievement.sol";

library BuyUpgrade {
    using GetCoins for Game;
    using GetAchievement for Game;

    function buyUpgrade(Game storage _g, AppStorage storage _s, uint8 _upgradeId) public {
        Upgrade memory localUpgrade;
        localUpgrade = _s.vars.getUpgrade(_upgradeId);
        int currentCookies = _g.getCoins();
        require(uint256(currentCookies) >= localUpgrade.price);
        require(_g.upgrades[_upgradeId] < 1 && _g.level >= localUpgrade.level);

        if(localUpgrade.upgDep > 0 ) {
            require(_g.upgrades[uint(uint16(localUpgrade.upgDep))] > 0);
        }
        if(localUpgrade.upgDep < 0 ) {
            uint upgDep = uint(uint16(localUpgrade.upgDep*-1));
            require(_g.upgrades[upgDep] == 0);
        }

        if(localUpgrade.achDep > 0) {
            require(_g.getAchievement(uint(uint16(localUpgrade.achDep))));
        }
        if(localUpgrade.achDep < 0) {
            uint achDep = uint(uint16(localUpgrade.achDep*-1));
            require(_g.getAchievement(achDep) == false);
        }
        //Upgrade sets new production value for the building
        if(localUpgrade.typeId == 0) {
            _g.profit[localUpgrade.building] += int16(localUpgrade.value % 2 == 0 ? localUpgrade.value : (localUpgrade.value * -1));
            //Pay
            _g.tokens = _g.tokens - (localUpgrade.price + (_g.getCoins() - currentCookies));
        }

        // //Give a certain amount of cookies
        if(localUpgrade.typeId == 1) {
            //Pay
            if(localUpgrade.value % 2 == 0) {
                _g.tokens = _g.tokens + (int(localUpgrade.value) - int(localUpgrade.price));
            } else {
                _g.tokens = _g.tokens - (int(localUpgrade.value) - int(localUpgrade.price));
            }
        }
        // //Reset balance destroys 
        if(localUpgrade.typeId == 2) {
            //Pay
            _g.tokens = 0;
        }
        // //Award amount of buildings
        if(localUpgrade.typeId == 3) {
            //Pay
            _g.buildingAmount[localUpgrade.building] = uint32(int32(_g.buildingAmount[localUpgrade.building] + localUpgrade.value));
            _g.buildingSum[localUpgrade.building] = _g.buildingSum[localUpgrade.building] + uint256(int(localUpgrade.value) *  int(block.number - _g.start));
            _g.tokens = _g.tokens - int(localUpgrade.price);
        }

        //Up
        if(localUpgrade.typeId == 4) {
            require(_g.getCoins() > localUpgrade.level * 500000);
            for (uint i=1; i<=20; i++) {
                _g.profit[i] += int16(localUpgrade.value);
            }
        }
        //shields once per game
        if(localUpgrade.typeId == 5) {
            uint achievementId = localUpgrade.building + 220;
            _g.achievements = _g.achievements | (uint256(1) << achievementId);
            for(uint i = 0; i<_s.gameIndex.length; i++) {
                Game storage g = _s.games[_s.gameIndex[i]];
                int result = 0;
                if(localUpgrade.value != 0) {
                    result = int(g.buildingAmount[localUpgrade.building]) + localUpgrade.value;
                }
                if(result <= 0) {       
                    g.buildingAmount[localUpgrade.building] = 0;
                    g.buildingSum[localUpgrade.building] = 0;
                } else {
                    g.buildingAmount[localUpgrade.building] = uint32(uint(result));
                    g.buildingSum[localUpgrade.building] = g.buildingSum[localUpgrade.building] / uint(result);
                }
            }
        }

        //Giveth doe er een
        if(localUpgrade.typeId == 6) {
            for(uint i = 0; i<_s.gameIndex.length; i++) {
                Game storage g = _s.games[_s.gameIndex[i]];
                uint32 target = uint32(int32(localUpgrade.value));
                g.buildingAmount[localUpgrade.building] = g.buildingAmount[localUpgrade.building] + target;
                g.buildingSum[localUpgrade.building] += (uint32(block.number - _g.start) * target);
            }
        }

        //Swap
        if(localUpgrade.typeId == 7) {
            uint256 sum = _g.buildingSum[localUpgrade.building];
            uint32 amount = _g.buildingAmount[localUpgrade.building];
            uint32 target = uint32(int32(localUpgrade.building) + int32(localUpgrade.value));

            _g.buildingAmount[localUpgrade.building] = _g.buildingAmount[target];
            _g.buildingSum[localUpgrade.building] = _g.buildingSum[target];

            _g.buildingAmount[target] = amount;
            _g.buildingSum[target] = sum;
        }

        //Swap random
        if(localUpgrade.typeId == 8) {
            uint256 sum = _g.buildingSum[localUpgrade.building];
            uint32 amount = _g.buildingAmount[localUpgrade.building];
            int32 random =  int32(uint32(_g.random) % 10) - 5;
            uint32 target = uint32(int32(localUpgrade.building) + int32(random));
            _g.buildingAmount[localUpgrade.building] = _g.buildingAmount[target];
            _g.buildingSum[localUpgrade.building] = _g.buildingSum[target];

            _g.buildingAmount[target] = amount;
            _g.buildingSum[target] = sum;
        }


        // Record it was bought
        _g.upgrades[_upgradeId] = uint32(block.number - _g.start);
    }
}