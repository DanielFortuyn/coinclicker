// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "../structs/Upgrade.sol";
import "./GetCoins.sol";
import "./GetAchievement.sol";

library ClaimAchievement {
    using GetCoins for Game;
    using GetAchievement for Game;

    function claimAchievement(Game storage _g, AppStorage storage _s, uint8 _achievementId) public {
        uint64 achievementValue = _s.vars.getAllAchievements()[_achievementId];
        bool b = false;
        uint count = 0;
        //Check deps
        if(_achievementId >= 0 && _achievementId < 10) {
            int tokens = _g.getCoins();
            require(tokens >= int(achievementValue), "N0F");
        }

        //Current prod
        if((_achievementId >= 10 && _achievementId < 20) || _achievementId == 129) {
            int total = 0;
            for(uint i=0; i<=20; i++) {
                if(_g.buildingAmount[i] > 0) {
                    total = total + ((int(_g.buildingAmount[i])) * _g.profit[i] * int(_g.level + 1));
                }
            }
            if(_achievementId != 129) {
                require(total > 0, 'O');
                require(total >= int(achievementValue), "NCF");
            } else {
                require(total <= int(achievementValue)*-1, "NNF");
            }
        }   

        //Check upgrade count
        if(_achievementId >= 20 && _achievementId < 30) {
            for(uint i = 0; i<=255; i++) {
                if(_g.upgrades[i] > 0) {
                    count++;
                }
            }
            require(count >= achievementValue, "N2F");
        }
        //Check achievement count
        if(_achievementId >= 30 && _achievementId < 40) {
            for(uint i = 0; i<=255; i++) {
                if(_g.getAchievement(i)) {
                    count++;
                }
            }
            require(count >= achievementValue, "N2F");
        }
        //Buildings 40 - 127
        if(_achievementId >= 40 && _achievementId <= 127)   {
            uint building = (_achievementId - 40);
            building = (building % 4 == 0) ? building : building-(building % 4);
            require(_g.buildingAmount[building/4] >= achievementValue, "N4F");
        }

        //Check Level
        if(_achievementId >= 130 && _achievementId < 140) { 
            require(_g.level >= achievementValue);
        }

        //Check CCGT Balance
        if(_achievementId >= 140 && _achievementId < 150) { 
            require(achievementValue > _s.tokens.balanceOf(msg.sender, 0) ** (10 ** 18));
        }

        //Spec update
        if(_achievementId >= 150 && _achievementId < 160) {
            require(_g.start > achievementValue, "AGE");
        }
        
        if(_achievementId >= 160 && _achievementId < 180) {
            require(_g.upgrades[achievementValue] > 0, "NUF");
        }

        // Each building
        if(_achievementId >= 180 && _achievementId < 185)   {
            uint32 lowest = 999;
            for(uint16 i = 0; i<20; i++) {
                if(_g.buildingAmount[i] < lowest) {
                    lowest = _g.buildingAmount[i];
                }
            }
            require(lowest > achievementValue);
        }
        if(_achievementId >= 185) {
            require(_g.upgrades[achievementValue] > 0, "NUF");
        }
        //220 - 244 shots fired
        b = true;
        if (b) {
           _g.achievements = _g.achievements | (uint256(1) << _achievementId);
        }
    }
}