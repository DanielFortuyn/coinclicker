// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "./GetCoins.sol";
import "./CoinMath.sol";
import "./NumHouses.sol";
import "../Tokens.sol";

library PullRug {
    using GetCoins for Game;    

    function pullRug(Game storage _g, AppStorage storage _s, uint8 _rugId) external isRug(_rugId) {
        uint256 rlevel = _s.tokens.getRugLevel()[_rugId-1];
        uint256 rprice = _s.tokens.getRugPrice()[_rugId-1];
        require(
            _g.getCoins() >= int256(rprice) &&
                _s.tokens.balanceOf(address(this), _rugId) > 0 &&
                uint256(_g.level) >= rlevel,
            "S"
        );
        _s.tokens.safeTransferFrom(address(this), msg.sender, _rugId, 1, "0x0");

        //Reset his/her game
        uint32[22] memory buildingAmount;
        uint256[22] memory buildingSum;
        uint256[22] memory buildingCost;
        uint32[128] memory upgrades;
        _g.profit = _s.vars.getProfit();
        _g.buildingSum = buildingSum;
        _g.buildingAmount = buildingAmount;
        _g.buildingCost = buildingCost;
        _g.buildingAmount[0] = NumHouses.numHouses(_s);
        _g.upgrades = upgrades;
        _g.start = block.number;
        _g.tokens = 0;
        _g.level = 0;
    }

    modifier isRug(uint _rugId) {
        if(_rugId>0 && _rugId <= 160) {
            _;
        }
    } 
}