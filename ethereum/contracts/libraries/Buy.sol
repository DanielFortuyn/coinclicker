// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "./GetCoins.sol";
import "./CoinMath.sol";

library Buy {
    using GetCoins for Game;
    function buy(Game storage g, AppStorage storage s, uint8 _buildingId) validBuyBuildingNumber(_buildingId) external  {
        int bprice;
        require(g.buildingAmount[_buildingId] < 300);
        if(_buildingId == 21) {
            uint amount = 1000;
            if(s.gameIndex.length < 60) {
                amount = 7000 - (s.gameIndex.length * 100);                
            }
            require(((block.number - s.sunrise)/amount) - s.bananas > 0);
            uint divide = s.gameIndex.length;
            if(s.gameIndex.length > 1) {
                divide = s.gameIndex.length / 2;
            }
            bprice = int(CoinMath.fracExp(s.vars.getPrice()[_buildingId], (s.bananas / divide)));
        } else {
            bprice = int(CoinMath.fracExp(s.vars.getPrice()[_buildingId], g.buildingAmount[_buildingId]));
        }
        require(g.getCoins() >= bprice, "X");
        g.buildingCost[_buildingId] += uint(bprice);
        g.buildingAmount[_buildingId]++;
        g.buildingSum[_buildingId] += ((block.number+1) - g.start);

        if(_buildingId  == 21) {
            s.bananas++;
        }
    }

    modifier validBuyBuildingNumber(uint buildingNumber) {
        if(buildingNumber>0 && buildingNumber<22) {
            _;
        }
    }
}
    