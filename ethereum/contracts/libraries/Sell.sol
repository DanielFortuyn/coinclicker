// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "./GetCoins.sol";

library Sell {
    using GetCoins for Game;

    modifier validSellBuildingNumber(uint buildingNumber) {
        if(buildingNumber>0 && buildingNumber<22) {
            _;
        }
    }

    function sell(Game storage _g, AppStorage storage _s, uint8 _buildingId) validSellBuildingNumber(_buildingId) external {
        require(_g.buildingAmount[_buildingId] > 0);

        if(_buildingId == 1) {
            //Sold grandma
            _g.achievements = _g.achievements | (uint256(1) << 253);
        }
        if(_buildingId == 7) {
            //Sold carlos
            _g.achievements = _g.achievements | (uint256(1) << 252);
        }

        int a = int(_g.buildingSum[_buildingId]) - (int(block.number - _g.start) / (_g.buildingAmount[_buildingId] + 1));
        if(a < 0) {
            _g.buildingSum[_buildingId] = 0;
        } else {
            _g.buildingSum[_buildingId] -= uint(a);
        }
        _g.buildingCost[_buildingId] = (_g.buildingCost[_buildingId] / (_g.buildingAmount[_buildingId]+1)) * _g.buildingAmount[_buildingId];
        _g.buildingAmount[_buildingId]--;
    }


}
    