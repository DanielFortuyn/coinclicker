//TBD// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";

library NumHouses {
    function numHouses(AppStorage storage _s) external view returns(uint32 n){
        n = uint32(_s.tokens.balanceOf(msg.sender, 0) / (500 * (10**uint256(18))))+1;
    }
}