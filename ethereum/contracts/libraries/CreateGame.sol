// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "./NumHouses.sol";

library CreateGame {
    function createGame(AppStorage storage _s, uint random) external {
        Game memory g;
        g.buildingAmount[0] = NumHouses.numHouses(_s);
        g.owner = msg.sender;
        g.start = block.number;
        g.profit = _s.vars.getProfit();
        g.random = random;
        g.claimed = block.number;
        _s.games[msg.sender] = g;
        _s.gameIndex.push(msg.sender);
        _s.gameCount++;
    }
}
    