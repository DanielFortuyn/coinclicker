// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";

library ClaimRandomEvent {

    event Claim(address indexed _from, uint indexed _block, uint _random, uint _randomslice, uint32 result);

    function claimRandomEvent(Game storage _g, AppStorage storage _s) public  {
        uint32 mod = uint32((_g.random % 1000) + 500);
        uint32 eventNumber = uint32(block.number % mod);
        require(eventNumber >= 0 && eventNumber <= 10, "NIW");

        //ICO 0
        if(mod <600) {
            _g.tokens = _g.tokens + int(mod * (50000 * (_g.level+1)));
        }

        //Bitconnect 1
        if(mod >= 600 && mod < 700) {
            if(mod%2==0) {
                _g.tokens = _g.tokens + int(mod * (200000 * (_g.level+1)));
            } else {
                _g.tokens = _g.tokens - int(mod * (100000 * (_g.level+1)));
            }
        }

        //Line dance2
        if(mod >= 700 && mod < 800) {
            if(mod%2==0) {
                _g.tokens = _g.tokens + int(mod * (200000 * (_g.level+1)));
            } else {
                _g.tokens = _g.tokens - int(mod * (100000 * (_g.level+1)));
            }
            _g.buildingAmount[1] += 4; 
            _g.buildingSum[1] += ((block.number - _g.start) * 4);
        }

        //Achievement3
        if(mod >= 800 && mod < 900) {
            _g.achievements = _g.achievements | (uint256(1) << 254);
        }

        //Prince4
        if(mod >= 900  && mod < 1000) {
            _g.achievements = _g.achievements | (uint256(1) << 255);
        }
        
        //Loot crate5
        if(mod >= 1000 && mod < 1100) {
            uint32 n = mod % 10;
            _g.buildingAmount[n] += n; 
            _g.buildingSum[n] += (block.number - _g.start)*n;
        }

        //Buy stonks 6
        if(mod >= 1100  && mod < 1200) {
             _g.buildingAmount[5] += mod%10; 
            _g.buildingSum[5] += ((block.number - _g.start) * (mod%10));
        }

        //Buy tokens
        if(mod >= 1200  && mod < 1300) {
            int pow = 3;
            if(_g.level > 5) {
                pow =_g.level - 2;
            }
             _g.tokens = _g.tokens + int(20^pow);
        }

         //Insurance
        if(mod >= 1300  && mod < 1400) {
             _g.tokens = _g.tokens - (_g.tokens/2);
        }

         //Inflation
        if(mod >= 1400  && mod < 1500) {
            if(mod%2 == 0) {
                _g.tokens = (_g.tokens * 5) / 4;
            } else {
                 _g.tokens = (_g.tokens * 4) / 5;
            }
        }
        _g.random = uint(blockhash(block.number - 1));
    } 
}