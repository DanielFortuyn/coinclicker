// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../structs/Game.sol";
import "../structs/AppStorage.sol";
import "../structs/Upgrade.sol";
import "./GetCoins.sol";
import "./GetAchievement.sol";

library ClaimGovernanceToken {
    using GetCoins for Game;
    using GetAchievement for Game;

    function claimGovernanceToken(Game storage _g, AppStorage storage _s) public {
        uint256 achievements = 0;
        for (uint256 i = 0; i <= 255; i++) {
            if (_g.getAchievement(i)) {
                achievements++;
            }
        }
        achievements = _s.vars.governanceBlockHeight() - (achievements * _s.vars.governanceAchievementReward());
        uint256 diff = (((block.number - (_g.claimed)) * (10**18)) / achievements);
        _g.claimed = block.number;
        require(_s.tokens.balanceOf(address(this), 0) > diff);
        _s.tokens.safeTransferFrom(address(this), msg.sender, 0, diff, "0x0");
    }
}