// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./structs/Upgrade.sol";
import "./structs/AppStorage.sol";

contract Vars is Initializable, OwnableUpgradeable {
    AppStorage internal s;    
    int32[22] public profit;
    uint64[22] public price;
    uint64[10] public levelPrice;
    uint64[256] public ach;

    mapping(uint32 => Upgrade) public upgs;

    function initialize() public initializer {   
        OwnableUpgradeable.__Ownable_init();

        profit = [int32(5),int32(10),int32(18),int32(34),int32(60),int32(105),int32(190),int32(340),int32(600),int32(1050),int32(1800),int32(3200),int32(5000),int32(8000),int32(12500),int32(20000),int32(31500),int32(37500),int32(57500),int32(100000),int32(-1000),int32(750)];
        price =  [9223372036854775807,1500,3500,9000,23000,55000,130000,320000,800000,2100000,5150000,12875000,33333333,81250000,213750000,512500000,1251287500,3125000000,7775000000,19437500000,70000,175000];
        levelPrice = [0,250000000,650000000,1500000000,8000000000,40000000000,600000000000,2400000000000,7000000000000,12000000000000];

        ach = [
            10000,1000000,50000000,10000000000,500000000000,100000000000000,10000000000000000,1000000000000000000,5000000000000000000,9000000000000000000,
            1000,10000,50000,100000,500000,1000000,10000000,100000000,1000000000,100000000000,
            1,5,10,20,30,40,50,60,80,100,
            10,20,30,40,50,60,80,100,111,256,
            5,10,20,50,5,10,20,50,5,10, 
            20,50,5,10,20,50,5,10,20,50, //52 WP
            5,10,20,50,5,10,20,50,5,10, 
            20,50,5,10,20,50,5,10,20,50,
            5,10,20,50,5,10,20,50,5,10, 
            20,50,5,10,20,50,5,10,20,50,
            5,10,20,50,5,10,20,50,5,10, 
            20,50,5,10,20,50,5,10,20,50,
            5,10,20,50,5,10,20,50,0,10, // 129
            0,1,2,3,4,5,6,7,8,9, // 130 - 139 level
            1,10,100,1000,5000,10000,50000,100000,250000,1000000, // CGT 140 - 149
            1,100,10000,1000000,100000000,100000000,10000000000,100000000000,1000000000000,1000000000000, // 150 - 159 AGE
            1,2,10,100,1000,10000,0,0,0,0, // 160 - 169 Negative production
            10000,20000,500000,1000000,50000000,0,0,0,0,0, // 170 - 179 Negative bank
            1,5,10,20,50,0,0,0,0,0, // 189
            0,0,0,0,0,0,0,0,0,0, // 199
            0,0,0,0,0,0,0,0,0,0, // 209
            0,0,0,0,0,0,0,0,0,0, // 219
            0,0,0,0,0,0,0,0,0,0, // 229
            0,0,0,0,0,0,0,0,0,0, // 239
            0,0,0,0,0,0,0,0,0,0, // 249
            0,0,0,0,0,0 // 256
        ];
    }

    uint16 public constant governanceAchievementReward = 30;
    uint32 public constant governanceBlockHeight = 80000;
    uint16 public constant bexponent = 4;
        

    //Type 
    // 0-9 Total tokens
    // 10-19 Total cpb
    // 20-29 Count upgrades
    // 30-39 Count achievements
    // 40-81 count buildings
    // 82-99 specific update 
    // 130-138 level

    function addUpgrades(uint32[] memory _key, Upgrade[] memory _upgradeData) public onlyOwner {
        
        Upgrade memory upg;
        
        for (uint32 i = 0; i < _key.length; i++) {
            upg.typeId = _upgradeData[i].typeId;
            upg.building = _upgradeData[i].building;
            upg.upgDep = _upgradeData[i].upgDep;
            upg.achDep = _upgradeData[i].achDep;
            upg.level = _upgradeData[i].level;
            upg.price = _upgradeData[i].price;
            upg.value = _upgradeData[i].value;
            upgs[_key[i]] = upg;
        }
    }

    function setAchievements(uint64[256] memory _achievements) public onlyOwner {
        ach = _achievements;
    }

    function getPrice() public view returns(uint64[22] memory) {
        return price;
    }
    function getProfit() public view returns(int32[22] memory) {
        return profit;
    }
    function getLevelPrice() public view returns(uint64[10] memory) {
        return levelPrice;
    }
    function getAllAchievements() public view returns(uint64[256] memory) {
        return ach;
    } 
    function getUpgrade(uint8 n) public view returns(Upgrade memory u) {
        return upgs[n];
    }
    function getUpgrades() public view returns (Upgrade[255] memory) {
        Upgrade[255] memory m;
        for(uint8 i=0; i<255; i++) {
            m[i] = upgs[i];
        }
        return m;
    }
}