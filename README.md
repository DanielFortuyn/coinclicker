# CoinClicker

I created this game in honor of cookieclicker! Basically it was a research project in order to understand solidity a bit better. That worked, now my aim is to release it to a proper layer 2 chain so people can actually play it.

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

# deploy contracts
npx hardhat run --network localhost scripts/deploy.ts 


# deploy contracts with current token contract
npx hardhat run --network localhost scripts/deploy.ts 