module.exports = {
    nFormatter(num, digits) {
        var si = [
            { value: 1, symbol: "" },
            { value: 1E3, symbol: "k" },
            { value: 1E6, symbol: "M" },
            { value: 1E9, symbol: "B" },
            { value: 1E12, symbol: "Qua" },
            { value: 1E15, symbol: "Qui" },
            { value: 1E18, symbol: "Non" },
            { value: 1E21, symbol: "Nonillion" }
        ];
        var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }
        return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    },
    pFormatter(num, digits) {
        return this.nFormatter(num / 100, digits)
    }
}