module.exports = [
    {
        level: 0,
        price: 0, 
        name: 'USDT',
        ticker: 'You have',
        underline: 'unbacked tokens',
        short: 'tokens',
        color: "#50af95",
        prod: 'Issueing',
    },
    {
        level: 1,
        price: 0, 
        name: 'ADA',
        ticker: 'You have less than',
        short: 'contracts',
        underline: 'smartcontracts'
    },
    {
        level: 2,
        price: 0, 
        name: 'BCH',
        ticker: 'You have been scammed for',
        underline: 'cash coins',
        short: 'coins',

    },
    {
        level: 3,
        price: 0, 
        name: 'DOGE',
        ticker: 'You bought',
        underline: 'meme coins',
        short: 'memes',
    },
    {
        level: 4,
        price: 0, 
        name: 'UNI',
        ticker: 'You received',
        underline: 'useless governance tokens',
        short: 'tokens',
    },
    {
        level: 5,
        price: 0, 
        name: 'LINK',
        ticker: 'You have conjured',
        underline: 'unstakeable oracle tokens',
        short: 'tokens'
    },
    {
        level: 6,
        price: 0, 
        name: 'BTC',
        ticker: 'You have mined',
        underline: '\'digital gold\'',
        short: 'gold',
    },
    {
        level: 7,
        price: 0, 
        name: 'MKR',
        ticker: 'You have deposited',
        underline: 'coletaral',
        short: 'coleteral',
    },
    {
        level: 8,
        price: 0, 
        name: 'DAI',
        ticker: 'You have minted',
        underline: 'decentralized stable tokens',
        short: 'tokens',
    },
    {
        level: 9,
        price: 0, 
        name: 'ETH',
        ticker: 'You should have',
        underline: 'staked ether',
        short: 'ether',
        color: "#f772d1"
    },
    {
        level: 10,
        price: 0, 
        name: 'ETH2',
        ticker: 'You are ready',
        underline: 'for lambo',
        short: 'the future',
        color: "#f772d1"
    },

]