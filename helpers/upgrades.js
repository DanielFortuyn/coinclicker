module.exports = [
    {
        id: 0,
        title: '0x0',
        description: '000',
        contract: {
            dependencies: {

            },
            price: 20000,
            typeId: 4,
            value: 40000
        },
        toast: {
            title: 'Segmentation fault',
            body: 'Dumping................ (memory)'
        },
    },
    {
        id: 11,
        title: 'Upgrade your home',
        description: 'Better home, better life, remote ok! ✌',
        contract: {
            dependencies: {
                achievement: 1
            },
            typeId: 0,
            building: 0,
            price: 1000,
            value: 4,
            level: 0
        },
        toast: {
            body: 'Look at all those rooms',
            title: 'Move teh bus!'
        },
    },
    {
        id: 12,
        title: 'Get a room!',
        description: 'Better home, many rooms, tokens and mice.',
        contract: {
            dependencies: {
                upgrade: 11
            },
            typeId: 0,
            building: 0,
            price: 10000,
            value: 8,
            level: 2  
        },
    },
    {
        id: 13,
        title: 'Move in with me',
        description: 'Best home, room for many leeches and grandmas',
        contract: {
            dependencies: {
                upgrade: 12
            },
            typeId: 0,
            price: 120000,
            building: 0,
            level: 1,
            value: 12,
   
        },
    },
    {
        id: 14,
        title: 'Gamer Grandma',
        description: 'Explain the game, so you do not have to stay up late for some ponzinomics',
        contract: {
            dependencies: {
                achievement: 20,
                upgrade: -191
            },
            typeId: 0,
            building: 1,
            price: 3000,
            value: 6,
            level: 0,
        }
    },
    {   
        id: 15,
        title: 'Working grandma',
        description: 'Make grandma work for her tokens.',
        contract: {
            dependencies: {
                upgrade: 14,
                achievement: 45
            },
            typeId: 0,
            building: 1,
            price: 30000,
            value: 8,
            level: 0,
        }
    },
    {
        id: 16,
        title: 'Stake your grandma',
        description: 'Put your grandma on the line, getting serious returns for a minor risk of liquidation',
        contract: {
            dependencies: {
                upgrade: 15,
                achievement: 46
            },
            typeId: 0,
            building: 1,
            price: 320000,
            value: 12,
            level: 1,
        }    
    },
    {
        id: 17,
        title: 'Larger wallet',
        description: 'A large wallet could potentially hold many tokens.',
        contract: {
            dependencies: {
                achievement: 20,
                upgrade: -191
            },
            typeId: 0,
            building: 2,
            price: 800000,
            value: 8,
            level: 0,
        },
        toast: {
            title: 'Useless',
            body: 'Why would you need it'
        },
    },
    {
        id: 18,
        title: 'Soft wallet',
        description: 'The first step to financial freedom and many scammy tokens.',
        contract: {
            dependencies: {
                upgrade: 17,
                achievement: 49
            },
            typeId: 0,
            building: 2,
            price: 5000000,
            value: 10,
            level: 1,
        },
    },
    {
        id: 19,
        title: 'Cold wallet',
        description: 'Full on paranoia, I guess',
        contract: {
            dependencies: {
                upgrade: 18,
                achievement: 50
            },
            typeId: 0,
            building: 2,
            price: 12500000,
            value: 12,
            level: 2,
        },
    },
    {
        id: 20,
        title: 'Yellow paper',
        description: 'Why do whitepapers if you can do yellow first.',
        contract: {
            dependencies: {
                achievement: 20,
                upgrade: -191,
            },
            typeId: 0,
            building: 3,
            price: 2500000,
            value: 10,
            level: 1,
        },

    },
    {
        id: 21,
        title: 'Brown paper',
        description: 'This is getting weird and out of control.',
        contract: {
            dependencies: {
                achievement: 53,
                upgrade: 20
            },
            typeId: 0,
            building: 3,
            price: 12500000,
            value: 12,
            level: 2,
        },

    },
    {
        id: 23,
        title: 'Mortage',
        description: 'Ahh I love the smell of capitalism and debt.',
        contract: {
            dependencies: {
                achievement: 20,
                upgrade: -191
            },
            typeId: 0,
            building: 4,
            price: 25000000,
            value: 12,
            level: 2,
        },

    },
    {
        id: 24,
        title: 'Large loan',
        description: 'Ahh I love the smell of capitalism and debt.',
        contract: {
            dependencies: {
                achievement: 54,
                upgrade: 23
            },
            typeId: 0,
            price: 12500000,
            building: 4,
            value: 16,
            level: 3,
        },

    
    },
    {
        id: 25,
        title: 'Vulture Capital',
        description: 'Ahh I love the smell of capitalism and debt.',
        contract: {
            dependencies: {
                achievement: 56,
            },
            typeId: 0,
            building: 4,
            price: 1500000,
            level: 20,
            value: 100
       },

    },
    {
        id: 27,
        title: 'ETF',
        description: 'Ahh I love the smell of capitalism and debt.',
        contract: {
            dependencies: {
                upgrade: -191,
                achievement: 60,
            },
            typeId: 0,
            building: 5,
            price: 5000000,
            value: 20,
            level: 2
        },

    },
    {
        id: 28,
        title: 'Techstonk',
        description: 'Ahh I love the smell of capitalism and debt.',
        contract: {
            dependencies: {
                achievement: 61,
                upgrade: 27,
            },
            typeId: 0,
            building: 6,
            price: 5000000,
            value: 30,
            level: 3
        },

    },
    {
        id: 29,
        title: 'Gamestonk',
        description: 'Ahh I love the smell of capitalism and debt.',
        contract: {
            dependencies: {
                upgrade: 28,
                achievement: 62,
            },
            typeId: 0,
            building: 6,
            price: 50000000,
            value: 40,
            level: 4,
        },

    },
    {
        id: 30,
        title: 'Truck',
        description: 'A new truck to improve mining a bit',
        contract: {
            dependencies: {
                achievement: 60,
                upgrade: -191
            },
            typeId: 0,
            building: 6,
            price: 50000000,
            value: 80,
            level: 4,
        },

    },
    {
        id: 31,
        title: 'Drill',
        description: 'A drill bit to mine a liquid token',
        contract: {
            dependencies: {
                upgrade: 30,
                achievement: 61,
            },
            typeId: 0,
            building: 6,
            price: 100000000,
            value: 160,
            level: 5,
        },

    },
    {
        id: 32,
        title: 'Fracking',
        description: 'Blow up all illiquid assets',
        contract: {
            dependencies: {
                upgrade: 31,
                achievement: 62,
            },
            typeId: 0,
            building: 6,
            price: 150000000,
            value: 240,
            level: 6,
        },
    },
    {
        id: 120,
        title: 'Friends',
        description: 'It is great to have friends, what will happen to your tokens though.',
        contract: {
            dependencies: {
                achievement: 1
            },
            level: 0,
            price: 500000,
            typeId: 3,
            building: 20,
            value: 1,
        },
        toast: {
            title: 'No way!',
            body: "This one guy, costs you ten tokens per block.. "
        }
    },
    {
        id: 121,
        title: 'Persistance',
        description: 'Nice one.',
        contract: {
            dependencies: {
                achievement: 121,
                upgrade: 120
            },
            typeId: 0,
            building: 20,
            price: 10000000,
            value: 1260
        }
    },
    {
        id: 122,
        title: 'Evil grandma',
        description: 'Not sure if...',
        contract: {
            dependencies: {
                achievement: 2,
                upgrade: 16
            },
            typeId: 0,
            price: 6660,
            building: 1,
            value: 105,
            level: 1,
        },

    },
    {
        id: 123,
        title: 'Retirement',
        description: 'Finally...',
        contract: {
            dependencies: {
                achievement: 46,
                upgrade: 122
            },
            typeId: 0,
            building: 1,
            price: 66666660,
            value: 140,
        }
    },
    {
        id: 125,
        title: 'Numba',
        description: 'Go up!',
        contract: {
            dependencies: {
                achievement: 5,
                upgrade: 10
            },
            price: 9999990,
            typeId: 0,
            building: 0,
            price: 9999990,
            value: 19,
            level: 4,
        }
    },
    { 
        id: 126,
        title: 'The real wolf',
        description: 'It\'s just fairy dust..',
        contract: {
            dependencies: {
                upgrade: 27
            },
            price: 9999990,
            typeId: 0,
            building: 4,
            price: 9999990,
            value: 21,
            level: 5,
        }
    },
    {
        id: 127,
        title: 'Rock',
        description: 'Buy a rock',
        contract: {
            dependencies: {
                achievement: 2
            },
            price: 10,
            typeId: 1,
            building: 0,
            price: 10000,
            value: 9,
            level: 0,
        },
        toast: {
            title: 'Trust me!',
            body: "At some level this is everything you need!"
        },
    },
    {
        id: 128,
        title: 'Rock',
        description: 'Sell a rock',
        contract: {
            dependencies: {
                upgrade: 127
            },
            price: 10,
            typeId: 1,
            building: 4,
            price: 100,
            value: 99999999,
            level: 2,
        },
        toast: {
            title: 'Smart move',
            body: "I guess"
        },
    },
    {
        id: 129,
        title: 'Eddie',
        description: 'What have you done for me lately!?',
        contract: {
            dependencies: {
                achievement: 255
            },
            price: 100000,
            typeId: 1,
            building: 4,
            value: 99999999,
            level: 2,
        },
        toast: {
            title: 'Zebra',
            body: "Bone-thru-her-nose"
        }
    },
    {
        id: 188,
        title: 'The lord giveth!',
        description: 'But could also take away',
        contract: {
            dependencies: {

            },
            typeId: 6,
            building: 7,
            price: 200000,
            value: 1,
            level: 3,
        },
        price: 10,
        toast: { 
            title: 'Wooooooooooooooooow',
            body: 'A wild carlos has appeared!'
        }
    },
    {
        id: 189,
        title: 'We are going in!',
        description: 'Don\'t look back, cuz there is no back',
        contract: {
            dependencies: {

            },
            typeId: 8,
            building: 6,
            price: 20000000,
            value: 1,
            level: 3,
        },
        price: 10,
        toast: { 
            title: 'It',
            body: 'Swaps!'
        }
    }, 
    {
        id: 190,
        title: 'Ooooohhh',
        description: 'What does...',
        contract: {
            dependencies: {
                achievement: -20
            },
            typeId: 7,
            building: 1,
            price: 200,
            value: 1,
            level: 0,
        },
        price: 10,
        toast: { 
            title: 'It',
            body: 'Swaps!'
        }
    },  
    {
        id: 191,
        title: 'Easy road',
        description: 'Take the easy road and win the game!',
        contract: {
            dependencies: {
                achievement: -20
            },
            typeId: 1,
            building: 1,
            price: 200,
            value: 500000,
            level: 0,
        },
        price: 10,
        toast: { 
            title: 'Thanks!',
            body: 'This was a maybe a great idea!'
        }
    },  
    {
        id: 192,
        title: 'Boomer',
        description: 'Get some help to win this game!',
        contract: {
            dependencies: {

            },
            typeId: 1,
            building: 1,
            price: 1000000000,
            value: 2555000000,
            level: 7,
        },
        level: 7,
        toast: { 
            title: 'Thanks!',
            body: 'This was a great idea!'
        }
    },  
    {
        id: 193,
        title: 'Scam dog',
        description: 'Here buy this awesome dog token!',
        contract: {
            dependencies: {

            },
            typeId: 1,
            building: 1,
            price: 1000000000,
            value: 1055000000,
            level: 6,
        },
        toast: { 
            title: 'Thanks!',
            body: 'This was a great idea!'
        }
    },    
    {
        id: 195,
        title: 'Inheritance',
        description: 'Well begun is, half done!',
        contract: {
            dependencies: {
                achievement: 253
            },
            level: 5,
            typeId: 1,
            building: 1,
            price: 10000000,
            value: 85000000,
            level: 0,
        },
        toast: { 
            title: 'Thanks!',
            body: 'This was a great idea!'
        }
    },
    { 
        id: 196,
        title: 'More botox',
        description: 'Something to help your granny a bit',
        contract: {
            dependencies: {
                upgrade: 198,
            },
            typeId: 0,
            building: 1,
            price: 1250000,
            value: 60,
            level: 0,
        },
        toast: {
            title: 'Yup! ',
            body: 'That is what you really want!'
        }
    },
    { 
        id: 197,
        title: 'Cow',
        description: 'Moooooooh',
        contract: {
            dependencies: {

            },
            typeId: 1,
            building: 1,
            price: 500000,
            value: 2000000,
            level: 5,
        },
        toast: {
            title: 'Ok ok,.. ',
            body: 'Is this what you really want?'
        }
    },
    { 
        id: 198,
        title: 'Botox',
        description: 'Something to help your granny a bit',
        contract: {
            dependencies: {

            },
            typeId: 0,
            building: 1,
            price: 150000,
            value: 11,
            level: 0,
        },
        price: 150,
        toast: {
            title: 'Ok ok,.. ',
            body: 'Is this what you really want?'
        }
    },
    { 
        id: 199,
        title: 'Starter pack',
        description: 'A starter pack, it will get you a granny',
        contract: {
            dependencies: {

            },
            typeId: 1,
            building: 1,
            price: 200,
            value: 4500,
            level: 0,
        },
        price: 200,
        toast: {
            title: 'Seriously',
            body: 'You understand? This game? 4reals?'
        }
    },
    {
        id: 201,
        title: "Teh money gun",
        description: 'Pulled it out',
        contract: {
            dependencies: {

            },
            typeId: 1,
            level: 0,
            price: 1000000,
            value: 5000000
        },
        extension: 'gif',
        toast: { 
            title: 'Epic moneyzzz',
            body: 'Nice work.. keep on looking there is way more stashed into this game!'
        }
    },
    {
        id: 202,
        title: 'Bogged',
        description: 'What they sold?!?!?.. ',
        contract: {
            dependencies: {

            },
            typeId:1,
            building: 2,
            price: 5000000,
            value: -10000,
            level: 2,
        },
        level: 2,
        toast: { 
            title: 'Dump it',
            body: 'Hello? Hellooo?!'
        }
    },    
    {
        id: 203,
        title: 'Bank',
        description: 'This looks like an interesting idea.. ',
        contract: {
            dependencies: {

            },
            level: 2,
            typeId: 0,
            building: 2,
            price: 5000000,
            value: -100000,
        }
    },
    {
        id: 204,
        title: 'ElSalvador',
        description: 'Maybe not the first, certainly not the last..',
        contract: {
            dependencies: {
                achievement: 4,
                upgrade: 205
            },
            level: 0,
            typeId: 3,
            building: 5,
            price: 5000000,
            value: 9,
        }
    },
    {
        id: 205,
        title: 'McAffee',
        description: 'Well, this is suprising',
        contract: {
            dependencies: {
                achievement: 1,
                upgrade: 206
            },
            level: 0,
            typeId: 3,
            building: 5,
            price: 50000,
            value: 5,
        }
    },
    {
        id: 206,
        title: 'When lambo',
        description: 'Sir, you have just received your very first lamborghini.',
        contract: {
            dependencies: {
                upgrade: -191
            },
            level: 0,
            typeId: 1,
            building: 0,
            price: 100,
            value: 50000
        }
    },
    {
        id: 207,
        title: 'Moon',
        description: 'Mooo00000ooon',
        contract: {
            dependencies: {
                achievement: 3
            },
            level: 1,
            typeId: 1,
            building: 1,
            price: 11110000,
            value: 111222233,
            level: 1,
        }
    },
    {
        id: 208,
        title: 'Bingo',
        description: 'Attract grannies for later use..',
        contract: {
            dependencies: {
                upgrade: -191
            },
            level: 0,
            typeId: 3,
            building: 1,
            price: 110000,
            value: 10
        }
    },
    {
        id: 209,
        title: 'Laser Carlos',
        description: 'Kids, this is why you don\'t do coke before going on stage.',
        contract: {
            dependencies: {

            },
            level: 1,
            typeId: 0,
            building: 7,
            price: 6000000,
            value: 270,
            level: 1
        }    
    },
    {
        id: 210,
        title: 'Funds Safu',
        description: 'Your funds are safe, but we\'ve reset the API keys,jk.',
        contract: {
            dependencies: {
                upgrade: -191
            },
            level: 0,
            typeId: 1,
            price: 20000,
            value: 300000,
            level: 0,
        }
    },
    {
        id: 211,
        title: '1 Up',
        description: 'Dinggg',
        contract: {
            dependencies: {

            },
            level: 1,
            typeId: 4,
            building: 1,
            price: 5000000,
            value: 10,
            level: 1,
        }
    },
    {
        id: 212,
        title: '2 Up',
        description: 'Dinggg',
        contract: {
            dependencies: {

            },
            level: 2,
            typeId: 4,
            price: 15000000,
            value: 40,
            level: 3,
        }
    },
    {
        id: 213,
        title: '3 Up',
        description: 'Dinggg',
        contract: {
            dependencies: {

            },
            level: 3,
            typeId: 5,
            building: 1,
            price: 1000000,
            value: 80,
            level: 4,
        }
    },
    {
        id: 214,
        title: 'Uh-oh',
        description: 'How about some nerf!',
        contract: {
            dependencies: {

            },
            level: 0,
            typeId: 5,
            building: 1,
            price: 1000000,
            value: 0,
            level: 0,
        },
        toast: { 
            title: 'Nuked!',
            body: 'What!? All grandmas have died?.. Nooooooo'
        }
    },
    {
        id: 215,
        title: 'The creator',
        description: 'How did they find me?!',
        contract: {
            dependencies: {

            },
            level: 0,
            typeId: 1,
            building: 1,
            price: 1000000,
            value: 10000000,
            level: 0,
        },
        toast: { 
            title: 'Thanks!',
            body: 'This was a great idea!'
        }
    },
    {
        id: 216,
        title: 'The cursor',
        description: 'Did you actually click the right one?',
        contract: {
            dependencies: {

            },
            level: 0,
            typeId: 1,
            building: 1,
            price: 1000,
            value: 20000,
            level: 0,
        },
        toast: { 
            title: 'Thanks!',
            body: 'Awesome, goodbye!!'
        }
    },
    {
        id: 217,
        title: 'The error',
        description: 'You sure you want this?',
        contract: {
            dependencies: {

            },
            level: 3,
            typeId: 1,
            building: 1,
            price: 1,
            value: 99999901,
            level: 0,
        },
        toast: { 
            title: 'Thanks!',
            body: 'Awesome, goodbye!!'
        }
    },
    {
        id: 218,
        title: 'The crate',
        description: 'Loot?',
        contract: {
            dependencies: {

            },
            level: 0,
            typeId: 1,
            building: 1,
            price: 50000000,
            value: 80000000,
            level: 0,
        },
        toast: { 
            title: 'Thanks!',
            body: 'Awesome, goodbye!!'
        }
    },
    {
        id: 219,
        title: 'Nigerian prince',
        description: 'Strikes back!',
        contract: {
            dependencies: {
                achievement: 255
            },
            level: 0,
            typeId: 1,
            building: 1,
            price: 5000000,
            value: 80000000,
            level: 0,
        },
        toast: { 
            title: 'Thanks!',
            body: 'Awesome, goodbye!!'
        }
    }
]