// import upgrades from "./upgrades";

module.exports = {
    findUpgradeById: function(upgrades, id) {
        for(let i = 0; i<upgrades.length; i++) {
            if(upgrades[i].id == id) {
                return upgrades[i];
            }
        }
        return false;
    },
    getAvailableAchievements: function (state, achievements) {
        let list = [];
        let upgCount = 0;
        let achCount = 0;
        let current = [...BigInt(state.achievements).toString(2)].reverse();
        if(state.maxCookies < 100) {
            return [];
        }
        for (let i = 0; i < 256; i++) {
            if (state.upgrades[i] > 0) {
                upgCount++;
            }
        }
        for (let i=0; i <= current.length; i ++) {
            if(current[i] == 1) {
                achCount++;
            }
        }
        let lowest = 999;
        for(let i = 0; i<20; i++) {
            if(state.buildings[i] < lowest) {
                lowest = state.buildings[i];
            }
        }

        for (let n = 0; n < achievements.length; n++) {
            let ach = achievements[n];
            let aid = ach.id;
            if(parseInt(current[aid]) == 1) {
                list.push(achievements[n]);
            } else {
                if (aid < 0) {
                    list.push(achievements[n]);
                }
                //Tel het aantal tokens
                if (aid >= 0 && aid <= 9) {
                    if (state.maxCookies > state.achievementData[aid]) {
                        list.push(achievements[n])
                    }
                }
                //Tel het aantal tokens per block
                if ((aid >= 10 && aid <= 19) || aid == 129) {
                    let total = 0;
                    for (let i = 0; i < 21; i++) {
                        total += (state.buildings[i] * state.profits[i]) * (state.level + 1);
                    }
                    if (aid != 129) {
                        if (total >= state.achievementData[aid]) {
                            list.push(achievements[n])
                        }
                    } else {
                        if (total < state.achievementData[aid] * -1) {
                            list.push(achievements[n])
                        }
                    }
                }
                //Tel het aantal upgrades
                if (aid >= 20 && aid < 29) {
                    if (upgCount >= state.achievementData[aid]) {
                        list.push(achievements[n]);
                    }
                }
                //Tel het aantal achievements
                if (aid >= 30 && aid < 39) {
                    if (achCount >= state.achievementData[aid]) {
                        list.push(achievements[n]);
                    }
                }
                if (aid >= 40 && aid < 128) {
                    let buildingId = Math.floor((aid - 39) / 4);
                    //Tel het aantal gebouwen uit de ach
                    // Friends
                    if (parseInt(state.buildings[buildingId]) >= parseInt(state.achievementData[aid])) {
                        list.push(achievements[n])
                    }
                }

                //Level
                if (aid >= 130 && aid < 140) {
                    if (parseInt(state.level) >= parseInt(state.achievementData[aid])) {
                        list.push(achievements[n])
                    }
                }

                //CCGT
                if (aid >= 140 && aid < 150) {

                }

                //AGE
                if (aid >= 150 && aid < 160) {
                    if (parseInt(state.start) >= parseInt(state.achievementData[aid])) {
                        list.push(achievements[n])
                    }
                }

                //Building total 180 -184
                if (aid >= 180 && aid < 184) {
                    if(lowest >= parseInt(state.achievementData[aid])) {
                        list.push(achievements[n])
                    }

                }
            }
        }

        return list;
    },
    getAvailableUpgrades: function (state, upgrades) {
        let list = [];
        if (state.upgradeData.length < 1) {
            return list;
        }
        return upgrades.filter(function (item) {
            let reason = ''
            let showUpdate = true;
            if(item.id > 200) {
                if (state.updateFilter[item.id - 200] == 0) {
                    reason = 'filter';
                    showUpdate = false;
                }
            }
            if (item.id > 0) {
                if(item.id == 200) {
                    if(state.cookies > 5) {
                        showUpdate = true;
                    }
                }
                if (showUpdate && parseInt(state.upgradeData[item.id][2]) > 0) {
                    if (state.upgrades[parseInt(state.upgradeData[item.id][2])] == 0) {
                        reason = 'upgrade';
                        showUpdate = false;
                    };
                }

                if (showUpdate && parseInt(state.upgradeData[item.id][2]) < 0) {
                    let upgdep = state.upgradeData[item.id][2] * -1;
                    if (parseInt(state.upgrades[upgdep]) != 0) {
                        reason = 'upgrade neg: ' + state.upgrades[parseInt(state.upgradeData[item.id][2])] + ' ' ;
                        showUpdate = false;
                    };
                }

                if (showUpdate && parseInt(state.upgradeData[item.id][3]) > 0) {
                    //Check achievement record
                    let record = parseInt(state.upgradeData[item.id][3]);
                    let bit = [...state.achievements.toString(2)].reverse()[record];
                    if (typeof bit == "undefined" || bit == "0") {
                        reason = 'achievement';
                        showUpdate = false;
                    }
                }

                if (showUpdate && parseInt(state.upgradeData[item.id][3]) < 0) {
                    //Check achievement record
                    let record = parseInt(state.upgradeData[item.id][3] * -1);
                    let bit = [...state.achievements.toString(2)].reverse()[record];
                    if (bit == "1") {
                        reason = 'achievement';
                        showUpdate = false;
                    }
                }
                if (showUpdate &&  parseInt(state.upgradeData[item.id][4]) > 0) {
                    //Check level
                    if (state.upgradeData[item.id][4] > state.level) {
                        reason = 'level';
                        showUpdate = false;
                    }
                }
                if(showUpdate && typeof item.price != 'undefined') {
                    if((state.maxCookies*100) < state.upgradeData[item.id][5]) {
                        reason = 'price';
                        showUpdate = false;
                    }
                }

            }
            // Always hide upgrade zero
            if(item.id == 0) {
                reason = '0';
                showUpdate = false;
            }
            // Show all bought upgrades always
            if (state.upgrades[item.id] > 0) {
                showUpdate = true;
            }
            // if(state.debug) {
                // console.log(item.id, showUpdate, reason)
            // }
            if (showUpdate) {
                return item;
            }
        });
        return list;
    }
}