import { connectors } from 'web3modal';
import mainData from "../ethereum/artifacts/contracts/Main.sol/Main.json";
import varData from "../ethereum/artifacts/contracts/Vars.sol/Vars.json";
import tokenData from "../ethereum/artifacts/contracts/Tokens.sol/Tokens.json";
import Web3 from "web3";
import Vue from 'vue';
import eventBus from '../helpers/eventBus';
import { nftData } from "../helpers/nfts";
import maincontracts from "../helpers/contracts";

let subscription;
let web3;
let gameContract;
let wssGameContract;
let govContract;
let varContract;
let getGameTimeout; 
let web3wss;

function getContracts(ctx) {
  if(typeof gameContract == 'undefined') {
    gameContract = new web3.eth.Contract(mainData.abi, ctx.state.contracts.CONTRACT_ADDRESS_MAIN);
  }
  if(typeof wssGameContract == 'undefined') {
    wssGameContract = new web3wss.eth.Contract(mainData.abi, ctx.state.contracts.CONTRACT_ADDRESS_MAIN);
  }
  if(typeof varContract == 'undefined') {
    varContract = new web3.eth.Contract(varData.abi, ctx.state.contracts.CONTRACT_ADDRESS_VARS);
  }
  if(typeof governanceToken == 'undefined') {
    govContract = new web3.eth.Contract(tokenData.abi, ctx.state.contracts.CONTRACT_ADDRESS_TOKENS);
  }
}

function checkPrice(state, price, level = false) {
  if(state.cookies < price) {
    eventBus.$emit('not-enough-token'); 
    return false;
  }
  if(level) {
    if(state.level < level) {
      eventBus.$emit('not-enough-level'); 
      return false;
    }
  }
  return true;
}

export const state = () => ({
  account: false,
  achievementData: [],
  achievements: [],
  balance: [0,0,0,0,0],
  banance: 0,
  buildings: [],
  blocktime: 15,
  block: 0,
  contract: false,
  contracts: maincontracts,
  compare: {},
  claimed: 0,
  cookies: BigInt(0),
  debug: false,
  decimals: 100,
  gasPrice: 10000000000,
  governanceAchievementReward: 80000,
  governanceBlockHeight: 5,
  rain: true,
  random: BigInt(0),
  randomEvent: false,
  randomEventReset: false,
  loaded: false,
  loading: false,
  numPlayers: 1,
  nftStart: 1,
  updateFilter: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
  sunrise: 0,
  playerList: [],
  prices: [],
  profits: [],
  levelPrices: [],
  upgrades: [],
  maxCookies: BigInt(0),
  level: 0,
  randomEvent: false,
  start: 0,
  upgradeData: [],
  governanceTokensInWallet: 0,
  networkError: false
})

export const mutations = {
  toggleRain(state) {
    state.rain = !state.rain;
  },
  setContracts(state, contracts) {
    state.contracts = contracts;
  },
  setAccount(state, account) {
    state.account = account;
  },
  setUpdateFilter(state, number) {
    let uf = [...state.updateFilter];
    uf[number] = 1;
    Vue.set(state,'updateFilter', uf);
  },
  setCookies(state, cookies) {
    state.cookies = cookies;
  },
  setMaxCookies(state, cookies) {
    state.maxCookies = cookies;
  },
  setCurrentProduction(state, cp) {
    state.currentProduction = cp;
  },
  incrementLevel(state) {
    state.level++;
  },
  setBlocktime(state, blocktime) {
    state.blocktime = blocktime;
  },
  setBalance(state, balance) {
    Vue.set(state, 'balance',balance);
  },
  setLevelPrices(state, levelPrices) {
    Vue.set(state, 'levelPrices',levelPrices);
  },
  toggleLoading(state) {
    state.loading = true;
  },
  setLoading(state) {
    state.loading = true;
  },
  resetLoading(state) {
    state.loading = false;
  },
  setContract(state, contract) {
    state.contract = contract;
  },
  setUpgrades(state, upgrades) {
    state.upgrades = upgrades;
  },
  increaseMaxCookies(state, cookies) {
    if(state.maxCookies < cookies) {
      state.maxCookies = cookies;
    }
  },
  setAchievementData(state, achievementData) {
    state.achievementData = achievementData;
  },
  increaseBuilding(state, buildingId) {
    let buildings = [...state.buildings];
    buildings[buildingId]++;
    if(buildingId == 21) {
      state.banance++;
    }
    Vue.set(state, 'buildings',buildings);
  },
  decreaseBuilding(state, buildingId) {
    let buildings = [...state.buildings];
    buildings[buildingId]--;
    Vue.set(state, 'buildings', buildings);
  },
  setCompare(state, data) {
    let currentCompare = JSON.parse(JSON.stringify(state.compare));
    currentCompare[data[0]] = data;
    Vue.set(state, 'compare', currentCompare);
  },
  setGovernanceBlockHeight(state, data) {
    state.governanceBlockHeight = data;
  },
  setGovernanceAchievementReward(state, data) {
    state.governanceAchievementReward = data;
  },
  setCurrentBlock(state, block) {
    state.block = block;
  },
  setUpgradeData(state, data) {
    Vue.set(state, 'upgradeData', data);
  },
  setGame(state, game) {
    state.buildings = game[7];
    state.profits = game[6];
    state.start = parseInt(game[3]);
    state.level = parseInt(game[1]);
    state.claimed = parseInt(game[2]);
    state.upgrades = game[9];
    state.achievements = Vue.set(state,'achievements', BigInt(game[4]));
    state.random = BigInt(game[10]);
    state.loaded = true;
  },
  setPrices(state, prices) {
    state.prices = prices;
  },
  setPlayerList(state, players) {
    Vue.set(state, 'playerList', players);
  },
  pushPlayerList(state, player) {
    state.playerList.push(player)
  },
  clearPlayerList(state) {
    Vue.set(state, 'playerList', []);
  },
  setNumPlayers(state, numPlayers) {
    state.numPlayers = numPlayers;
  },
  setGovernanceTokens(state, data) {
    state.governanceTokensInWallet = data
  },
  setBanance(state, data) {
    state.banance = parseInt(data)
  },
  setSunrise(state, data) {
    state.sunrise = parseInt(data)
  },
  setRandomEvent(state, data) {
    state.randomEvent = data;
    state.randomEventReset = data;
  },
  setNetworkError(state) {
    state.networkError = true;
  },
  setGasPrice(state, data) {
    state.gasPrice = data;
  }
}

function dec2bin(dec) {
  return (dec >>> 0).toString(2);
}

export const getters = {
  achievementCount: state => {
      let achievementCount = 0;
      [...state.achievements.toString(2)].map((item) => {
        if (item != 0) {
          achievementCount++;
        }
      });
      return achievementCount;
  },
  govTokenProgress: (state, getters) => {
    return getters.govTokenCount - Math.floor(getters.govTokenCount);
  },
  age: state => {
    return (state.block -  (state.claimed));
  },
  govTokenCount: (state, getters) => {
    return Math.round((getters.age / getters.blockreward) * 10000) / 10000;
  },
  blockreward: (state, getters) => {
    return (
      state.governanceBlockHeight - getters.achievementCount * state.governanceAchievementReward
    );
  },
}

export const actions  = {
  async initContract(ctx, {provider}) {
    web3wss = new Web3(new Web3.providers.WebsocketProvider("wss://wsapi.fantom.network/"));
    ctx.commit('setLoading');
    web3 = new Web3(provider)
    const accounts = await web3.eth.getAccounts();
    ctx.commit('setAccount',  accounts[0]);
    getContracts(ctx);
    ctx.dispatch("getUpgradeData");
    ctx.dispatch("getGame");
    ctx.dispatch('getPrices');
    ctx.dispatch('getLevelPrices');
    ctx.dispatch('getAchievementData');
    ctx.dispatch('getDaoData');
    ctx.dispatch('getSunrise');
    ctx.dispatch('fetchGovernanceBalance');
    ctx.dispatch('getNetworkSpeed');

    const subscription = web3wss.eth.subscribe('newBlockHeaders', (error, blockHeader) => {
      if (error) return console.error(error);
      ctx.dispatch('updateEachBlock');
      ctx.commit('setCurrentBlock', blockHeader.number);

    });
  },
  async updateEachBlock(ctx) {
    getContracts(ctx);
    const currentNumber = ctx.state.block + 1;

    clearTimeout(getGameTimeout);
    if(!ctx.state.loading) {
      ctx.commit('setCurrentBlock', currentNumber);
      ctx.dispatch('fetchGovernanceBalance');
      ctx.dispatch('getBanance');
      ctx.dispatch('getCookies');
      ctx.commit('setGasPrice',await web3wss.eth.getGasPrice());

      if(currentNumber % 100 == 0) {
        ctx.dispatch('getNetworkSpeed');
      }

      
      let mod = (parseInt(ctx.state.random.toString().substring(ctx.state.random.toString().length-3)) + 500);
      let eventId = Math.floor((mod/100)-5);
      // console.log(ctx.state.random.toString(), currentNumber, mod,  currentNumber % mod, eventId) ;
      if(currentNumber % mod < 5 && !ctx.state.randomEvent) {
        ctx.commit('setRandomEvent', true);
        eventBus.$emit('random-event', eventId);
      }
      if(currentNumber % mod > 5 && ctx.state.randomEvent) {
        ctx.commit('setRandomEvent', false);
        eventBus.$emit('random-event-hide');
      }
    }
  },
  async getGame(ctx) {
    let gc = new web3wss.eth.Contract(mainData.abi, ctx.state.contracts.CONTRACT_ADDRESS_MAIN);
    try {
      ctx.commit('setGame', await gc.methods.getGame(ctx.state.account).call({
        from: ctx.state.account
      }));
    } catch(e) {
      console.log("Nosetgame", e);
      ctx.commit('setNetworkError');
    }

    ctx.commit('resetLoading');
  },
  async getNetworkSpeed(ctx) {
    const span = 10;
    const times = [];
    const currentNumber = await web3.eth.getBlockNumber();
    ctx.commit('setCurrentBlock', currentNumber);

    const firstBlock = await web3.eth.getBlock(currentNumber - span);
    let prevTimestamp = firstBlock.timestamp;
    for (let z = currentNumber - span + 1; z <= currentNumber; z++) {
        if (web3) {
            const block = await web3.eth.getBlock(z);
            let time = block.timestamp - prevTimestamp;
            prevTimestamp = block.timestamp;
            times.push(time);
        }
    }
    ctx.commit('setBlocktime', times.reduce((a, b) => a + b) / times.length);
  },
  async fetchGovernanceBalance(ctx) {
    let data = await govContract.methods.balanceOf(ctx.state.account, 0).call({
      from: ctx.state.account
    });
    ctx.commit('setGovernanceTokens', data);
  },
  async fetchPlayers(ctx) {
    const response = await this.$axios.$get('https://api.fureweb.com/spreadsheets/1dUrbmHgPVvodCitCRmmxRHiph-09YJJLaT8M6vP0fGQ');
    if(response.data){
      let addresses = response.data.map((i) => {
        return i["Paste your address in this column"];
      });
      ctx.commit('clearPlayerList');
      ctx.commit('setPlayerList', addresses);
    }
  },
  async claimGovernanceTokens(ctx) {
    try {
      await gameContract.methods
        .claimGovernanceToken()
        .send({
          from: ctx.state.account,
        })
        .catch((e) => {
          console.log("Caughit it", e);
        });
    } catch (e) {
      console.log(e);
    }
  },
  async getRugBalance(ctx) {
    getContracts(ctx);
    let balance = [0];
    let addresses = [];
    let token = [];

    for(let i=1; i<=160; i++) {
      addresses.push(ctx.state.contracts.CONTRACT_ADDRESS_MAIN);
      token.push(i);
    };

    let bal = await govContract.methods.balanceOfBatch(addresses, token).call({
      from: ctx.state.account,
    });
    bal = bal.map(function(val, index) {
      return parseInt(val, 0);
    });
    let merge = [0].concat(bal);
    ctx.commit('setBalance', merge);
  },
  async unsubscribe(ctx) {
    if (subscription) {
      subscription.unsubscribe(function (error, success) {
        if (success)
          console.log("Successfully unsubscribed from web3 updates!");
      });
    }
  },
  async getNumPlayers(ctx) {
    ctx.commit('setNumPlayers', await gameContract.methods.getNumPlayers().call({
      from: ctx.state.account
    }));
  },
  refreshCompare(ctx) {
    for (const [key, value] of Object.entries(ctx.state.compare)) {
      ctx.dispatch('loadAddress', value[0]);
    }
  },
  async loadAddress(ctx, address) {
    let game = await gameContract.methods.getGame(address).call({
      from: ctx.state.account
    });
    if(game[0] != "0x0000000000000000000000000000000000000000") {
      ctx.commit('setCompare', game);
    }
  },
  async getPrices(ctx) {
    ctx.commit('setPrices', await varContract.methods.getPrice().call({
      from: ctx.state.account
    }));
  },
  async getLevelPrices(ctx) {
    ctx.commit('setLevelPrices', await varContract.methods.getLevelPrice().call({
      from: ctx.state.account
    }));
  },
  async getUpgradeData(ctx) {
    try {
      ctx.commit('setUpgradeData', await varContract.methods.getUpgrades().call({
        from: ctx.state.account
      }));
    } catch(e) {
      ctx.commit('resetLoading');
    } 
  },
  async getAchievementData(ctx) {
    ctx.commit('setAchievementData', await varContract.methods.getAllAchievements().call({
      from: ctx.state.account
    }));
  },
  async getDaoData(ctx) {
    ctx.commit('setGovernanceBlockHeight', await varContract.methods.governanceBlockHeight().call({
      from: ctx.state.account
    }));
    ctx.commit('setGovernanceAchievementReward', await varContract.methods.governanceAchievementReward().call({
      from: ctx.state.account
    }));
  },
  async getBanance(ctx) {
    ctx.commit('setBanance', await gameContract.methods.banance().call({
      from: ctx.state.account
    }));
  },
  async getSunrise(ctx) {
    ctx.commit('setSunrise', await gameContract.methods.sunrise().call({
      from: ctx.state.account
    }));
  },
  async claimEvent(ctx) {
    await gameContract.methods.claimRandomEvent().send({
      from: ctx.state.account,
      gasPrice: ctx.state.gasPrice * 1.5
    });
  },
  async createGame(ctx) {
    ctx.commit('toggleLoading');
    await gameContract.methods.createGame().send({
      from: ctx.state.account,
    });
    ctx.commit('resetLoading');
  },
  async getCookies(ctx) {
    const cookies = await wssGameContract.methods
        .getCoins()
        .call({ from: ctx.state.account });
    ctx.commit('setCookies', parseInt(cookies));
    ctx.commit('increaseMaxCookies', parseInt(cookies));
  },
  async levelUp(ctx) {
    ctx.commit('toggleLoading');
    try {
      await gameContract.methods.levelUp().send({
        from: ctx.state.account,
      }).catch((e) => {
        console.log("Levelled it", e);
      });
      ctx.commit('incrementLevel');
    } catch (e) {
      console.log(e);
    }
    ctx.commit('resetLoading');
  },
  async build(ctx, {buildingId, amount}) {
    let price = Math.ceil(ctx.state.prices[buildingId] * Math.pow(1.375, ctx.state.buildings[buildingId]));
    if(checkPrice(ctx.state, price, false)) {
      ctx.commit('toggleLoading');
      try {
        ctx.commit('increaseBuilding', buildingId);
        ctx.commit('setCookies', ctx.state.cookies - price);
        await gameContract.methods
          .buy(buildingId)
          .send({
            from: ctx.state.account,
          })
          .catch((e) => {
            console.log("Caughit it", e);
          });
        ctx.dispatch('getGame');
      } catch (e) {
        console.log(e);
      }
      ctx.commit('resetLoading');
    }
  },
  async sell(ctx, {buildingId, amount}) {
    ctx.commit('toggleLoading');
    try {
      ctx.commit('decreaseBuilding', buildingId);
      await gameContract.methods
        .sell(buildingId)
        .send({
          from: ctx.state.account,
        })
        .catch((e) => {
          console.log("Caughit it", e);
        });
    } catch (e) {
      console.log(e);
    }
    ctx.commit('resetLoading');
    ctx.dispatch('getGame');
  },
  async pullRug(ctx, rugId) {
    if(checkPrice(ctx.state, nftData[rugId-1].properties.price, nftData[rugId-1].properties.level)) {
      ctx.commit('toggleLoading');
      try {
        await gameContract.methods
          .pullRug(rugId)
          .send({
            from: ctx.state.account,
          })
          .catch((e) => {
            console.log("Caughit it", e);
          });
      } catch (e) {
        console.log(e);
      }
      ctx.commit('resetLoading');
      ctx.dispatch('getGame');
      ctx.dispatch('getBanance');
    }
  },
  async setCookies(ctx, cookies) {
    ctx.commit('toggleLoading');
    try {
      await gameContract.methods
        .setCookies(cookies)
        .send({
          from: ctx.state.account,
        })
        .catch((e) => {
          console.log("Caughit it", e);
        });
        ctx.commit('setCookies', cookies);

    } catch (e) {
      console.log(e);
    }
    ctx.commit('resetLoading');
  },
  async sendTip(ctx) {
    await web3.eth.sendTransaction({
      to: "0x49DB82FF7B06976F2ab6B69C5d23A6c7ee0BcEa3",
      from: ctx.state.account,
      value: web3.utils.toWei('0.01', 'ether'),
    }) 
  },
  async setLevel(ctx, level) {
    ctx.commit('toggleLoading');
    try {
      await gameContract.methods
        .setLevel(level)
        .send({
          from: ctx.state.account,
        })
        .catch((e) => {
          console.log("Caught it", e);
        });
      ctx.state.commit('setLevel', level);
    } catch (e) {
      console.log(e);
    }
    ctx.commit('resetLoading');
  },
  async buyUpgrade(ctx, id) {
    let price = ctx.state.upgradeData[id][5];
    if(checkPrice(ctx.state, price, false)) {
      ctx.commit('toggleLoading');
      try {
        await gameContract.methods
          .buyUpgrade(id)
          .send({
            from: ctx.state.account,
          })
          .catch((e) => {
            console.log("Caught it", e);
          });
          eventBus.$emit('toast-upgrade-success', id);
      } catch (e) {
        console.log(e);
      }
      ctx.commit('resetLoading');
    }
  },
  async claimAchievement(ctx, id) {
    ctx.commit('toggleLoading');
    try {
      await gameContract.methods
        .claimAchievement(id)
        .send({
          from: ctx.state.account,
        })
        .catch((e) => {
          console.log("Caughit it", e);
        });
    } catch (e) {
      console.log(e);
    }
    ctx.commit('resetLoading');
  }
}

